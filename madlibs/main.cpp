//
//  main.cpp
//  mad-lips-demo
//
//  Created by Kevin H. Patterson on 1/21/19.
//  Copyright © 2019 Creltek. All rights reserved.
//

#include <iostream>
#include <string>
#include <sstream>

using namespace std;

int main(int argc, const char * argv[]) {
    cout << "Welcome to the Mad Lips demo!" << endl;
    
    // create the story
    stringstream ss;
    ss << "Dear <RELATIVE>, I am having a(n) <ADJECTIVE> time at camp.";
    ss << "The counselour is <ADJECTIVE 1> and the food is <ADJECTIVE 2>.";
    ss << "I met <PERSON IN ROOM 1> and we became <ADJECTIVE 3> friends.";
    ss << "Unfortunately, <PERSON IN ROOM 1> is <ADJECTIVE 4> and I <VERB ENDING IN 'ED'> my <BODY PART> so we couldn`t go <VERB ENDING IN 'ING'> like everybody else.";
    ss << " I need more <NOUN (PLURAL)> and a <NOUN> sharpener, so please <ADVERB> <VERB> more when you <VERB 2> back." << endl;
    ss << "Your <RELATIVE 2>," << endl << "<PERSON IN ROOM 2>";

    // convert it to one big string
    string s = ss.str();
    
    // find the first <
    size_t pos = s.find( "<" );
    while( pos != string::npos ) {
        
        // find the closing >
        size_t pos2 = s.find( ">" );
        if( pos2 == string::npos )
            pos2 = s.size();
        
        // get the placeholder e.g. <PLACEHOLDER>
        string part = s.substr( pos, pos2 - pos + 1 );
        
        // display the placeholder without the < and >
        std::cout << part.substr( 1, part.size() - 2 ) << ": ";
        
        // get the user's response (replacement for the placeholder)
        string response;
        std::cin >> response;
        
        // replace the placeholder in the string with the user's response
        s.replace( pos, pos2 - pos + 1, response );

        // find the next <
        pos = s.find( "<" );
    }

    // display the story
    cout << s << endl;
    
    return 0;
}
